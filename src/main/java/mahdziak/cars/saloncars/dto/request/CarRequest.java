package mahdziak.cars.saloncars.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CarRequest {


    private String color;

    private Integer year;

    private Double volume;

    private String engine;

    private Integer maxSpeed;

    private Integer acceleration;

    private Boolean newCar;


    private Long bodyTypeId;

    private Long modelId;


}
