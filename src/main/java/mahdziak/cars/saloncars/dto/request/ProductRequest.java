package mahdziak.cars.saloncars.dto.request;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ProductRequest {

    private String name;

    private Integer price;

    private Integer priceWithDiscount;

    private Boolean isReserved;

    private Boolean isSold;

    private Boolean isDiscount;

    private LocalDate dateAdded;


    private Long countryId;
    private CountryRequest country;

    private Long carId;
    private CarRequest car;



}
