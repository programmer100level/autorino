package mahdziak.cars.saloncars.dto.request;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Getter
@Setter
@NoArgsConstructor
public class UserRequest {

    @NotNull
    @Size(min = 4,max = 16)
    private String firstName;

    @NotNull
    private String lastName;

//    @Max(60)
//    @Min(18)
    private String birthDate;

    private String country;

    private String login;

    private String password;





}
