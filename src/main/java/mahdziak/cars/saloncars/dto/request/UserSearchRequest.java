package mahdziak.cars.saloncars.dto.request;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class UserSearchRequest {

    private String firstName;

    private String lastName;

    private String birthDate;

    private String country;




    private PaginationRequest paginationRequest;
}
