package mahdziak.cars.saloncars.dto.response;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mahdziak.cars.saloncars.entity.Product;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ProductResponse {

    private Long id;

    private String name;

    private Integer price;

    private Integer priceWithDiscount;

    private Boolean isReserved;

    private Boolean isSold;

    private Boolean isDiscount;

    private LocalDate dateAdded;



    private Long countryName;

//    private Long carId;
    private CarResponse car;




    public ProductResponse(Product product) {
        id = product.getId();
        name = product.getName();
        price = product.getPrice();
        priceWithDiscount = product.getPriceWithDiscount();
        isReserved = product.getIsReserved();
        isSold = product.getIsSold();
        isDiscount = product.getIsDiscount();
        dateAdded = product.getDateAdded();
        car = new CarResponse(product.getCar());
//        country = new CountryResponse(product.getCountry());
        countryName = product.getCountry().getId();


    }

}
