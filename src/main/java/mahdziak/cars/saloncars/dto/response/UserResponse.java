package mahdziak.cars.saloncars.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mahdziak.cars.saloncars.entity.User;

@Getter
@Setter
@NoArgsConstructor
public class UserResponse {

    private Long id;

    private String firsName;

    private String lastName;

    private String birthDate;

    private String country;

    private String login;



    public UserResponse(User user) {
        id = user.getId();
        firsName = user.getFirstName();
        lastName = user.getLastName();
        birthDate = user.getBirthDate();
        login = user.getLogin();
        country = user.getCountry();
    }
}
