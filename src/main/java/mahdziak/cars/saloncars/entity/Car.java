package mahdziak.cars.saloncars.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.bytebuddy.build.ToStringPlugin;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString

@Entity
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;

    private String color;

    private Integer year;

    private Double volume;

    private String engine;

    private Integer maxSpeed;

    private Integer acceleration;

    private Boolean newCar;



    @OneToOne(mappedBy = "car")
    private Product product;

    @ManyToOne
    private BodyType bodyType;

    @ManyToOne
    private Model model;


}
