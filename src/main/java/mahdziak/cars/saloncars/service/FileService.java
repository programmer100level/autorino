package mahdziak.cars.saloncars.service;

import mahdziak.cars.saloncars.dto.request.FileRequest;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.UUID;

@Service
public class FileService {

    public static final String IMG_DIR =
            System.getProperty("user.home") + File.separator +
                    "cars-images";

    public Path saveFile(FileRequest request) throws IOException {
        createDir(IMG_DIR);

        String[] data = request.getData().split(",");
        String metaInfo = data[0];
        String base64File = data[1];

        return Files.write(createFileDestination(request.getFileName(),
                getFileExtensionFromMetaInfo(metaInfo)),
                Base64.getDecoder().decode(base64File.getBytes())
        );


    }

    private Path createFileDestination(String fileName, String fileExtension) {
        if (fileName == null) {
            fileName = UUID.randomUUID().toString();
        }

        return Paths.get(IMG_DIR, String.format("%s.%s", fileName, fileExtension));
    }

    private String getFileExtensionFromMetaInfo(String metaInfo) {
        return metaInfo.split("/")[1].split(";")[0];
    }

    private void createDir(String dir) {
        File file = new File(dir);
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}
