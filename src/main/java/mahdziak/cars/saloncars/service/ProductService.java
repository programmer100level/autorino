package mahdziak.cars.saloncars.service;

import mahdziak.cars.saloncars.Repository.ProductRepository;
import mahdziak.cars.saloncars.dto.request.*;
import mahdziak.cars.saloncars.dto.response.DataResponse;
import mahdziak.cars.saloncars.dto.response.ProductResponse;
import mahdziak.cars.saloncars.entity.Car;
import mahdziak.cars.saloncars.entity.Country;
import mahdziak.cars.saloncars.entity.Product;
import mahdziak.cars.saloncars.exception.WrongInputException;
import mahdziak.cars.saloncars.specification.ProductSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    private CarService carService;


    public ProductResponse save(ProductRequest productRequest) throws WrongInputException {
        return new ProductResponse(productRequestToProduct(null, productRequest));
    }


    private Product productRequestToProduct(Product product, ProductRequest request) throws WrongInputException {
        if (product == null) {
            product = new Product();
        }
        product.setName(request.getName());
        product.setPrice(request.getPrice());
        product.setPriceWithDiscount(request.getPriceWithDiscount());
        product.setDateAdded(request.getDateAdded());
        product.setIsDiscount(request.getIsDiscount());
        product.setIsReserved(request.getIsReserved());
        product.setIsSold(request.getIsSold());
        product.setCountry(countryService.findOne(request.getCountryId()));
        product.setCar(carService.findOne(request.getCarId()));


        return productRepository.save(product);
    }


    public DataResponse<ProductResponse> findByFilter(ProductFilterRequest productFilterRequest) {
        Page<Product> page = productRepository.findAll(
                new ProductSpecification(productFilterRequest),
                productFilterRequest.getPagination().mapToPageRequest());

        return new DataResponse<>(page.get().map(ProductResponse::new).collect(Collectors.toList()), page.getTotalPages(), page.getTotalElements());

    }

    public DataResponse<ProductResponse> findAllByCountryId(Long countryId, PaginationRequest paginationRequest) throws WrongInputException {
        Country country = countryService.findOne(countryId);
        Page<Product> byCountry = productRepository.findAllByCountry(country, paginationRequest.mapToPageRequest());
        return new DataResponse<>(byCountry.get().map(ProductResponse::new).collect(Collectors.toList()),
                byCountry.getTotalPages(), byCountry.getTotalElements());
    }
    public DataResponse<ProductResponse> findAllByCarId(Long carId, PaginationRequest paginationRequest) throws WrongInputException {
        Car car = carService.findOne(carId);
        Page<Product> byCar = productRepository.findAllByCar(car, paginationRequest.mapToPageRequest());
        return new DataResponse<>(byCar.get().map(ProductResponse::new).collect(Collectors.toList()),
                byCar.getTotalPages(), byCar.getTotalElements());
    }


    public Product findOne(Long id) throws WrongInputException {
        return productRepository.findById(id).orElseThrow(() -> new WrongInputException("Product with id " + id + " not exists"));
    }


    public ProductResponse update(Long id, ProductRequest productRequest) throws WrongInputException {
        return new ProductResponse(productRequestToProduct(findOne(id), productRequest));
    }


    public List<ProductResponse> findAll() {
        return productRepository.findAll().stream().map(ProductResponse::new).collect(Collectors.toList());
    }


}
