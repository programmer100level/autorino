package mahdziak.cars.saloncars.service;

import mahdziak.cars.saloncars.Repository.UserRepository;
import mahdziak.cars.saloncars.dto.request.UserSearchRequest;
import mahdziak.cars.saloncars.dto.response.DataResponse;
import mahdziak.cars.saloncars.dto.response.UserResponse;
import mahdziak.cars.saloncars.entity.User;
import mahdziak.cars.saloncars.exception.WrongInputException;
//import mahdziak.cars.saloncars.specification.UserSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

//import mahdziak.cars.saloncars.security.tokenUtils.TokenTool;
//import org.springframework.security.crypto.password.PasswordEncoder;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
//
//    @Autowired
//    private PasswordEncoder passwordEncoder;
//
//    @Autowired
//    private TokenTool tokenTool;

//
//    public Long saveUser(UserRequest request){
//        User user = new User();
//        user.setFirstName(request.getFirstName());
//        user.setLastName(request.getLastName());
//        user.setBirthDate(request.getBirthDate());
//        user.setCountry(request.getCountry());
//        user = userRepository.save(user);
//        return user.getId();
//    }


    public void delete(Long id) throws WrongInputException {
        userRepository.delete(findOne(id));
    }




    public User findOne(Long id) throws WrongInputException {
        return userRepository.findById(id)
                .orElseThrow(() -> new WrongInputException("User with id " + id + " not exists"));
    }

    public List<UserResponse> findAll() {
        return userRepository.findAll().stream().map(UserResponse::new).collect(Collectors.toList());
    }
//
//    public DataResponse<UserResponse> findAllByFilter(UserSearchRequest request) {
//        Page<User> page = userRepository.findAll(
//                new UserSpecification(request),
//                request.getPaginationRequest().mapToPageRequest()
//        );
//        return new DataResponse<>(page.get().map(UserResponse::new).collect(Collectors.toList()),
//                page.getTotalPages(), page.getTotalElements());
//    }

//
//    public String save(UserRequest request) throws Exception {
//        if (userRepository.findByLoginEquals(request.getLogin()).isPresent()) {
//            throw new Exception("Credentials are busy. Please, try one more time " +
//                    "with other login");
//        }
//
//        User user = new User();
//        user.setFirstName(request.getFirstName());
//        user.setLastName(request.getLastName());
//        user.setBirthDate(request.getBirthDate());
//        user.setCountry(request.getCountry());
//        user.setLogin(request.getLogin());
//        user.setPassword(passwordEncoder.encode(request.getPassword()));
//        user.setRole(UserRoles.CUSTOMER);
//
//        user = userRepository.saveAndFlush(user);
//
//        return tokenTool.createToken(user.getLogin(), user.getRole().name());
//    }

//
//
//    public String findOneByRequest(UserWithLoginRequest userRequest) throws WrongInputDataException {
//        User user = userRepository.findByLoginEquals(userRequest.getLogin()).orElseThrow(() -> new WrongInputDataException("User with login " + userRequest.getLogin() + " not exists"));
//
//        if (passwordEncoder.matches(userRequest.getPassword(), user.getPassword())) {
//            return tokenTool.createToken(user.getLogin(), user.getRole().name());
//        }
//
//        throw new IllegalArgumentException("Wrong login or password");
//    }


//     @Transactional
//    public UserResponse findOne(Long id){
//        Optional<User> userOptional = userRepository.findById(id);
//        if(userOptional.isPresent()){
//            return new UserResponse(userOptional.get());
//        }else{
//            throw new IllegalArgumentException("User with id "+id+" not found");
//        }
//    }


}