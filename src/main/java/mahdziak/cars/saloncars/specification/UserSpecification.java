package mahdziak.cars.saloncars.specification;

import mahdziak.cars.saloncars.entity.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class UserSpecification implements Specification<User> {
    private String value;

    public UserSpecification(String value) {
        this.value = value;
    }

    private Predicate findByFirstName(Root<User> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.like(root.get("firstName"), value);
    }

    private Predicate findByLastName(Root<User> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.like(root.get("lastName"), value);
    }
    private Predicate findByCountry(Root<User> root, CriteriaBuilder criteriaBuilder){
        return criteriaBuilder.like(root.get("country"), value);
    }
    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.or(findByFirstName(root, criteriaBuilder), findByLastName(root, criteriaBuilder), findByCountry(root, criteriaBuilder));
    }
}

